/**
Initialize DOM
*/

DOM = {};

$(document).ready(function() {
    /**
    INTERFACE
    */
    DOM.body = $('body');
    DOM.sections = DOM.body.find('section');
    DOM.nav = DOM.body.find('nav');
    DOM.chatInfo = DOM.body.find('aside');
    DOM.contacts = DOM.nav.find('#contacts');
    DOM.minprofile = DOM.nav.find('#minprofile');
    DOM.search_action = DOM.nav.find('#search_action');

    DOM.setH = function() {
        $('output:visible').height(
            $('section:visible').outerHeight()
            -
            (($('header:visible')[0]) ? ($('header:visible').outerHeight()) : 0)
            -
            (($('footer:visible')[0]) ? ($('footer:visible').outerHeight()) : 0)
        );
    }
    DOM.reinitialise = function(timeout,interval) {

        if (typeof timeout == "undefined") timeout = 0;

        if (typeof interval !== "undefined") {
            id = setInterval(DOM.reinitialise, interval);
            setTimeout("clearInterval("+id+")",timeout);
            return;
        }

        if (!timeout) DOM.setH();
        else setTimeout(DOM.setH,timeout);

        if (typeof Skar.pages[Skar.page] != "undefined" && typeof Skar.pages[Skar.page].scroll !== "undefined") {
            var scroll = Skar.pages[Skar.page].scroll;

            if (!timeout) scroll.reinitialise();
            else setTimeout(scroll.reinitialise,timeout);
        }
    }
    DOM.lswipeOpen = function() {
        DOM.body.css({'paddingLeft':'250px','paddingRight':'0px'});
        DOM.chatInfo.css('zIndex','-1');
        DOM.body.find('.lswipe').find('.infoicon').addClass('closeicon');

        DOM.reinitialise(310,31);
    }
    DOM.lswipeClose = function() {
        DOM.body.css('paddingLeft','0px');
        DOM.body.find('.lswipe').find('.infoicon').removeClass('closeicon');

        DOM.reinitialise(310,31);
    }

    DOM.rswipeTrigger = function() {
        if (DOM.body.css('paddingRight') == '0px') {
            DOM.body.css({'paddingRight':'250px','paddingLeft':'0px'})
            DOM.chatInfo.css('zIndex','0')
        } else {
            DOM.body.css('paddingRight','0px')
        }
        DOM.body.find('.lswipe').find('.infoicon').removeClass('closeicon');

        DOM.reinitialise(310,31)
    }
    DOM.rswipeOpen = function() {

        DOM.body.css({'paddingRight':'250px','paddingLeft':'0px'})
        DOM.chatInfo.css('zIndex','0')

        DOM.body.find('.lswipe').find('.infoicon').removeClass('closeicon');

        DOM.reinitialise(310,31)
    }

    DOM.body.on("click", '.lswipe', function() {
        if (DOM.body.css('paddingLeft') == '0px') {
            DOM.lswipeOpen();
        } else {
            DOM.lswipeClose();
        }
    });

    // Глобальные обработчики
    window.onload = function() {
        DOM.reinitialise();
    }
    $(window).resize(function() {
        DOM.reinitialise();
    });

    // Вкладка получила фокус
    $(document).on('show', function() {
        var contact = DOM.contacts.children('.current');
        if (typeof contact.attr("data-newmsg") !== "undefined")
            Skar.unread--;
        DrawFavicon(Skar.unread);
        contact.removeAttr("data-newmsg").addClass("current");
    });

    window.addEventListener("popstate", function(e) {
        if (Skar.authorized)
            openFromUrl();
    });
});
