
/* Requirements */
DOM = {};
Templates = {};

var host = window.location.host;

/**
LOGIC
*/
Skar = {
    authorized: false,
    socket: null,
    requests:{},
    user:{
        name: 'Anonimus',
        login: "skaro::"+host+"/@Anonimus"
    },
    contacts: [],
    unread: 0,

    // DOM objects and scroll api object for loaded pages
    pages:{
        loginpage:{
            dom:$('#loginpage'),
            scroll:undefined
        },
        registerpage:{
            dom:$('#registerpage'),
            scroll:undefined
        }
    },
    // Name and id current page
    page:"loginpage",

    // Initialise connection and e.t.c.
    start: function() {
        var url = "ws://" + host + "/skaro_client/";
        Skar.socket = new WebSocket(url,["skaro"]);
        Skar.socket.binaryType = 'arraybuffer';
        Skar.socket.onopen = function() {
            show_notice('success', "Соединение установлено", 5);
            Skar.openConnection();
        };
        Skar.socket.onerror = function () {
            var loader = DOM.body.find("#loginpage .mainloader:visible");
            if (loader[0])
                loader.addClass("stop");
            show_notice('error', "Не удалось установить соединение");
        };
        Skar.socket.onclose = function() {
            show_notice('error', "Соединение потеряно",25);
        };
        Skar.socket.onmessage = function(event) {
            var data = CBOR.decode(event.data);
            console.log(data);

            switch(data.type) {
                // Обработка ответов на запросы к серверу
                case "request":
                    if (typeof Skar.requests[data.chain_uuid] !== "undefined")
                        Skar.requests[data.chain_uuid].success(data);
                    break;
                // Сервер в чем-то ошибся.
                case "error":
                    show_notice('error', data.status + "<br/>" + data.info, 60);
                    break;
                // Сервер хочет что-то нам сказать
                case "info":
                    show_notice('information', data.status + "<br/>" + data.info, 60);
                    break;
                // Запрос сервера на отображение нового сообщения
                case "received_message":
                    Skar.receivedMessage(data, false);
                    break;
                // Кто-то, кого мы знаем, поменял публичные данные
                case "changed_user_data":
                    Skar.changedUserData(data);
                    break;
                default:
                    console.log('Пришло сообщение неизвестного типа');
                    break;
            }
        };
    },
    // Обработчик открытия соединения
    openConnection: function() {
        var session_id = getCookie("SID");
        if (!session_id) {
            Skar.RegisterType();
        } else {
            Skar.sessionRestore(session_id);
        }
    },
    openLoginform: function(register_type) {
        // Скрываем лоадер и показываем форму авторизации
        Skar.pages.loginpage.dom.html(Template.loginform(register_type)).promise().done(function(){
            $('section').hide();
            $('section#loginpage').show();

            // Форма авторизации
            Skar.pages.loginpage.dom.find('#loginform').off("submit").on("submit", function() {
                Skar.Authorize($(this));
                return false;
            });
            Skar.pages.loginpage.dom.find('#reset_pass').on("click", function() {
                console.log('reset');
                return false;
            });
            if (register_type == 'open' || register_type == 'invite' && getUrlParams().invite) {
                Skar.pages.loginpage.dom.find('#register').on("click", function() {
                    Skar.openRegform(register_type);
                    return false;
                });
            }
        });
    },
    openRegform: function(register_type) {
        // Скрываем лоадер и показываем форму авторизации
        Skar.pages.registerpage.dom.html(Template.registerform()).promise().done(function(){
            $('section').hide();
            $('section#registerpage').show();

            $('[name="email"]').bind("keyup change", function(e) {
                login = e.target.value.split("@")
                $('#registerform').find('[name="login"]').val(login[0]);
            })

            // Форма авторизации
            Skar.pages.registerpage.dom.find('#registerform').off("submit").on("submit", function() {
                Skar.Register($(this), register_type);
                return false;
            });
            Skar.pages.registerpage.dom.find('#signin').on("click", function() {
                Skar.openLoginform(register_type);
                return false;
            });
        });
    },
    Authorize: function(form) {
        var data = {};
        data.type = "authorize";
        data.login = form.toDict().login;
        var password = form.toDict().password;

        // Говорим серверу, что нужно ответить на запрос.
        // И устанавливаем обработчик события "прихода" ответа сервера.
        data.chain_uuid = new Request({
            // Сколько ждать (сек)
            timeout: 10,
            // Пришел ответ сервера
            success: function(request) {
                var form = $('#loginform');
                if (request.status.substr(0,3) === "200") {

                    // Пытаемся расшифровать принятый хэш
                    var res = {
                        'type': "request",
                        'status': "200 OK",
                        'info': ""
                    }

                    // 1. Клиент достаёт из локального хранилища свой запароленный приватный ключ
                    var private_key = localStorage.getItem("private_key");

                    // 2. Клиент расшифровывает введённым паролем свой запароленный приватный ключ
                    var crypt = new OpenCrypto();
                    crypt.decryptPrivateKey(private_key, password).then(function(decryptedPrivateKey) {
                        crypt.cryptoPrivateToPem(decryptedPrivateKey).then(function(privatePem) {

                            // 3. C помощью приватного ключа клиент расшифровывает RND и отсылает на сервер хэш SHA256(RND)
                            var decrypt = new JSEncrypt();
                            decrypt.setPrivateKey(privatePem);
                            var rnd = decrypt.decrypt(request.encrypted_rnd);
                            res.rnd_hash = CryptoJS.SHA256(rnd).toString();

                            // Говорим серверу, что нужно ответить на запрос.
                            // И устанавливаем обработчик события "прихода" ответа сервера.
                            res.chain_uuid = new Request({
                                // Сколько ждать (сек)
                                timeout: 10,
                                // Пришел ответ сервера
                                success: function(req) {
                                    var form = $('#loginform');
                                    if (req.status.substr(0,3) === "200") {
                                        // Меняем интерфейс к событию правильной авторизации
                                        form.find('[name="login"]').removeClass('invalid').addClass('valid').val("");
                                        form.find('[name="password"]').removeClass('invalid').addClass('valid').val("");
                                        $.noty.closeAll();

                                        // Получаем права на отправку браузерных оповещений
                                        Notification.requestPermission();

                                        // Выполняет первичные операции для только что вошедшего юзера
                                        Skar.initUser(req);
                                    } else {
                                        form.find('[name="login"]').removeClass('valid').addClass('invalid').val("");
                                        form.find('[name="password"]').removeClass('valid').addClass('invalid').val("");
                                        show_notice('error', req.info, 20);
                                    }
                                },
                                // Ошибка
                                error: function(code) {
                                    if (typeof code == "undefined") code = 'Время вышло, сервер не ответил.';
                                    show_notice('error', code, 20);
                                    console.log(code);
                                    console.log(this);
                                }
                            }, request.chain_uuid).chain_uuid;

                            Skar.socket.send(CBOR.encode(res));

                        });
                    });

                } else {
                    form.find('[name="login"]').removeClass('valid').addClass('invalid').val("");
                    form.find('[name="password"]').removeClass('valid').addClass('invalid').val("");
                    show_notice('error', request.info, 20);
                }
            },
            // Ошибка
            error: function(code) {
                if (typeof code == "undefined") code = 'Время вышло, сервер не ответил.';
                show_notice('error', code, 20);
                console.log(code);
                console.log(this);
            }
        }).chain_uuid;

        Skar.socket.send(CBOR.encode(data));
        form.find('[name="login"]').val("");
        form.find('[name="password"]').val("");
    },
    Register: function(form, register_type) {
        var data = {};

        if (register_type == 'invite' && getUrlParams().invite)
            data.invite = getUrlParams().invite;

        data.type = "register";

        var raw = form.toDict();
        data.email = raw['email'];
        data.login = raw['login'];

        // 1. Клиент выбирает себе логин и пароль
        // 2. Клиент генерирует ключевую пару RSA.
        //    Публичный ключ e,N и приватный ключ d,N
        var crypt = new OpenCrypto();
        crypt.getKeyPair().then(function(keyPair) {
            crypt.cryptoPrivateToPem(keyPair.privateKey).then(function(privatePem) {
                crypt.cryptoPublicToPem(keyPair.publicKey).then(function(publicPem) {
                    data.public_key = publicPem;

                    // 3. Клиент устанавливает пароль на приватный ключ
                    var password = raw['password'];
                    crypt.encryptPrivateKey(keyPair.privateKey, password).then(function(encryptedPrivateKey) {

                        // 4. Клиент сохраняет в локальном хранилище свой публичный ключ и запароленный приватный ключ
                        localStorage.setItem('public_key', data.public_key);
                        localStorage.setItem('private_key', encryptedPrivateKey);

                        // 4. Клиент отсылает на сервер, а сервер сохраняет — логин, E-Mail, открытый ключ e,N.

                        // Говорим серверу, что нужно ответить на запрос.
                        // И устанавливаем обработчик события "прихода" ответа сервера.
                        data.chain_uuid = new Request({
                            // Сколько ждать (сек)
                            timeout: 10,
                            // Пришел ответ сервера
                            success: function(request) {
                                var form = $('#registerform');
                                if (request.status.substr(0,3) === "200") {
                                    // Меняем интерфейс к событию правильной авторизации
                                    form.find('[name="email"]').removeClass('invalid').addClass('valid');
                                    form.find('[name="login"]').removeClass('invalid').addClass('valid');
                                    form.find('[name="password"]').removeClass('invalid').addClass('valid');
                                    $.noty.closeAll();

                                    // Т.к. регистрация прошла успешно, авторизуем пользователя
                                    Skar.openLoginform();
                                    Skar.pages.loginpage.dom.find('[name="login"]').val(request.login);
                                    Skar.pages.loginpage.dom.find('[name="password"]').val(form.find('[name="password"]').val());
                                    Skar.pages.loginpage.dom.find('#loginform').submit();
                                } else {
                                    form.find('[name="email"]').removeClass('valid').addClass('invalid');
                                    form.find('[name="login"]').removeClass('valid').addClass('invalid');
                                    form.find('[name="password"]').removeClass('valid').addClass('invalid');
                                    show_notice('error', request.info, 20);
                                }
                            },
                            // Ошибка
                            error: function(code) {
                                if (typeof code == "undefined") code = 'Время вышло, сервер не ответил.';
                                show_notice('error', code, 20);
                                console.log(code);
                                console.log(this);
                            }
                        }).chain_uuid;

                        Skar.socket.send(CBOR.encode(data));
                    });
                });
            });
        });
    },
    RegisterType: function() {
        var data = {};

        data.type = "register_type";

        // Говорим серверу, что нужно ответить на запрос.
        // И устанавливаем обработчик события "прихода" ответа сервера.
        data.chain_uuid = new Request({
            // Сколько ждать (сек)
            timeout: 10,
            // Пришел ответ сервера
            success: function(request) {
                var form = $('#loginform');
                if (request.status.substr(0,3) === "200") {
                    $.noty.closeAll();

                    if (request.register_type == 'invite' && getUrlParams().invite) {
                        Skar.openRegform(request.register_type);
                    } else {
                        Skar.openLoginform(request.register_type);
                    }
                } else {
                    show_notice('error', request.info, 20);
                }
            },
            // Ошибка
            error: function(code) {
                if (typeof code == "undefined") code = 'Время вышло, сервер не ответил.';
                show_notice('error', code, 20);
                console.log(code);
                console.log(this);
            }
        }).chain_uuid;

        Skar.socket.send(CBOR.encode(data));
    },
    // Восстанавливает соединение по идентификатору предыдущей сессии соединения
    sessionRestore: function(session_id) {
        var data = {
            type: 'session_restore',
            session_id: session_id,
            // Говорим серверу, что нужно ответить на запрос.
            // И устанавливаем обработчик события "прихода" ответа сервера.
            chain_uuid: new Request({
                // Сколько ждать (сек)
                timeout: 10,
                // Пришел ответ сервера
                success: function(request) {
                    if (request.status.substr(0,3) === "200") {
                        // Выполняет первичные операции для только что вошедшего юзера
                        Skar.initUser(request);
                    } else {

                        // Узнаём возможность регистрации и открываем форму авторизации
                        Skar.RegisterType();

                        // Сообщаем, что не удалось восстановить сессию
                        show_notice('warning', request.info, 60, [
                            {
                                addClass: 'green-text',
                                text: 'ПОВТОРИТЬ',
                                onClick: function ($noty) {
                                    // this = button element
                                    // $noty = $noty element

                                    $noty.close();

                                    Skar.sessionRestore(getCookie("SID"));
                                }
                            },
                            {
                                addClass: 'red-text',
                                text: 'ОТМЕНА',
                                onClick: function ($noty) {
                                    $noty.close();
                                    deleteCookie("SID");
                                }
                            }
                        ]);
                    }
                },
                // Ошибка
                error: function(code) {
                    console.log(this);
                    if (typeof code == "undefined") code = 'Время вышло, сервер не ответил.';
                    show_notice('error', code, 30);
                }
            }).chain_uuid
        }

        Skar.socket.send(CBOR.encode(data));

    },
    // Инициализирует юзера(выполняет первичные операции входа в акк)
    initUser: function(request) {
        // Запоминаем id сессии в куках.
        setCookie("SID",request.session_id);

        // Обновляем данные юзера
        Skar.user = request.user;
        Skar.user.login = request.user.login;
        Skar.authorized = true;

        // Выводим первые изменения на странице
        // Обновляем мини-профиль
        DOM.minprofile.html(Template.minprofile(
            typeof Skar.user.name !== "undefined" && Skar.user.name ? Skar.user.name : Skar.user.login,
            'Доступен',
            '/img/noavatar.png'
        ));

        Skar.getContacts().done(function(contacts) {
            DOM.contacts.on("click"," > div", function() {
                var el = $(this);
                Skar.openDialog(el.data("login"));
            });

            DOM.search_action.on("click", function() {
                Skar.openSearchContact(this.value);
                $("#search_field").focus();
            });

            // Открываем левый свайп и открываем профиль юзера
            DOM.lswipeOpen();

            // Открываем страницу согласно URL
            openFromUrl();
        })

    },
    // Решает куда выводить и выводит новое сообщение
    // todo: необходимо разделять логин комнаты и логин приходящего сообщения,
    // todo: чтобы организовать поддержку конференций
    receivedMessage: function(message, is_wait) {
        if (typeof is_wait == "undefined") is_wait = true;

        var login;

        // Если сообщение принадлежит нам
        if (message.from.login == Skar.user.login) {
            login = message.to;
        // Иначе, это сообщение не ОТ нас, а К нам.
        } else {
            if (typeof message.from.in_conference == "undefined")
                login = message.from.login;
            else
                login = message.from.in_conference;
        }

        // Получаем данные юзера, который отправил это сообщение
        Skar.getUserData(login).always(function(data) {
            var user;
            // Неизвестный пользователь, тогда в data строка с ошибкой
            if (this.state() == "rejected") {
                user = Skar.initUserData({
                    login:login,
                    name:login,
                    status:"offline",
                    current_info:data
                });
            } else {
                user = data;
            }

            // Ищем страницу с диалогом
            var pageid = 'dialog'+user.uid;

            // Уведомляем пользователя
            if ((pageid !== Skar.page)||document.hidden) {
                var contact = DOM.contacts.find('#'+user.uid);
                var old = 0;
                if (typeof contact.attr("data-newmsg") !== "undefined") {
                    old = parseInt(contact.attr("data-newmsg"));
                } else {
                    Skar.unread++;
                }
                contact.attr("data-newmsg",(old+1));
                DrawFavicon(Skar.unread);
                if (document.hidden)
                    SendNotification({title: user.name, body: message.body, login: user.login});
            }

            // Если не нашли страницу диалога, то создаем её.
            if (typeof Skar.pages[pageid] === "undefined") {
                // Эта функция создает диалог и автоматически загружает историю, если она есть.
                Skar.createDialog(user,pageid);
            // Если диалог уже существует то добавляем сообщение
            } else {
                var scroll = Skar.pages[pageid].scroll;
                var chat = Skar.pages[pageid].dom;

                // Проверка, чтобы сообщения не дублировались
                var existing = chat.find("#m" + message.mid);
                if (existing.length > 0) return;

                // Получаем HTML сообщения
                var node = $(Template.message(
                    message,
                    is_wait,
                    chat.find('output .message:last-child').data('login') === message.from.login
                ));

                // Есть ли скролл до добавления нового сообщения
                var scrollactive = false;
                if (scroll.getIsScrollableV())
                    scrollactive = true;

                // Добавляем сообщение
                chat.find('output').find('.jspPane').append(node);

                scroll.reinitialise();
                // Перематываем в конец, если с этим сообщением появился скролл.
                if (scrollactive === false && scroll.getIsScrollableV())
                    scroll.scrollToBottom();
            }

        });
    },
    // Загружаем историю сообщений в pageid
    updateHistory: function(login,pageid) {
        Skar.pages[pageid].dom.find('output').html(Template.loading());
        var data = {
            type: "get_history",
            login: login,
            chain_uuid: new Request({
                // Сколько ждать (сек)
                timeout: 10,
                // Пришел ответ сервера
                success: function(request) {
                    if (request.status.substr(0,3) === "200") {
                        var html = [],
                            before = '';
                        for(var i in request.history) {
                            html.push(Template.message(
                                request.history[i],
                                false,
                                request.history[i].from.login === before
                            ));
                            before = request.history[i].from.login;
                        }

                        var output = Skar.pages[this.data.pageid].dom.find('output');
                        output.html(html.join(''))
                        var outputscroll = output.jScrollPane({
                            stickToBottom: true,
                            contentWidth:100
                        });
                        Skar.pages[this.data.pageid].scroll = outputscroll.data('jsp');

                        DOM.reinitialise();

                        Skar.pages[this.data.pageid].scroll.scrollToBottom();
                    } else {
                        this.error(request.info);
                    }
                },
                // Ответ не получен
                error: function(code) {
                    if (typeof code == "undefined") code = 'Время вышло, сервер не ответил.';
                    Skar.pages[this.data.pageid].dom.find('output').html(Template.errorForOutput(code,'"Skar.updateHistory('+this.data.login+'",'+this.data.pageid+')'));
                    show_notice('error', code, 30);
                },
                data: {pageid:pageid,login:login}
            }).chain_uuid
        };
        Skar.socket.send(CBOR.encode(data));
    },
    // Отправляет сообщение
    sendMessage: function(form,login) {
        var
            data = form.toDict(),
            // Генерируем временный id для сообщения
            todo_mid = Uniq.getID();

        data.type = "send_message";
        data.to = login;
        data.chain_uuid = new Request({
            // Сколько ждать (сек)
            timeout: 10,
            // Пришел ответ сервера
            success: function(request) {
                if (request.status.substr(0,3) === "200") {
                    // Заменяем временное id для клиента, на реальное id сообщения в БД
                    $('#w'+this.data.todo_mid)
                        .removeClass('waiting')
                        .attr("id",'m'+request.mid)
                    .find('div.footer')
                        .html($("<time/>",{
                            text: request.date
                        }))
                } else {
                    this.error(request.info);
                }
            },
            // Ответ не получен
            error: function(code) {
                if (typeof code == "undefined") code = 'Время вышло, сервер не ответил.';
                $('#w'+this.data.todo_mid)
                    .removeClass('waiting')
                    .addClass('error')
                .find('div.footer')
                    .text(code);
                /* TODO: тут нужно сделать добавление сообщению кнопки "попробовать еще раз" с нужным событием. */
            },
            data: {todo_mid:todo_mid}
        }).chain_uuid;

        Skar.receivedMessage({
            mid:todo_mid,
            to:login,
            from:{
                name: Skar.user.name,
                login: Skar.user.login
            },
            date:'sending...',
            body:data.body
        });

        DOM.reinitialise();

        Skar.socket.send(CBOR.encode(data));
        form.find('textarea').val("").select();
    },
    // Обновляет профиль пользователя
    updateMyProfile: function() {

        // Заглушка с лоадером
        Skar.pages.myprofile.dom.html(Template.loading());

        // Показываем страницу настройки
        $('section').hide();
        Skar.pages.myprofile.dom.show();
        Skar.page = "myprofile";

        Skar.getUserData(Skar.user.login,false).done(function(user) {
            // Заменяем лоадер, на саму страницу
            Skar.pages.myprofile.dom.html(Template.myprofile(user));
            // Создаем обработчик скролла
            var outputscroll = Skar.pages.myprofile.dom.find('output').jScrollPane({
                stickToBottom: true,
                contentWidth:100
            });
            Skar.pages.myprofile.scroll = outputscroll.data('jsp');

            // Обновляем данные о юзере
            Skar.user = user;
        }).fail(function() {
            Skar.pages.myprofile.dom.html(Template.myprofile(Skar.user));
        })

        /*scroll.scrollToBottom();*/
    },
    setMyProfileData: function(update) {
        // Создаем Deferred обьект, для того, чтобы выводить результат функции в callback
        var deferred = $.Deferred(),
            out = deferred.promise()

        var data = {
            type: "update_my_data",
            update: update,
            chain_uuid: new Request({
                // Сколько ждать (сек)
                timeout: 10,
                data: {update: update},
                // Пришел ответ сервера
                success: function(request) {
                    if (request.status.substr(0,3) === "200") {
                        Skar.user = $.extend(Skar.user, this.data.update);
                        // Обновляем мини-профиль
                        DOM.minprofile.html(Template.minprofile(
                            typeof Skar.user.name !== "undefined" && Skar.user.name ? Skar.user.name : Skar.user.login,
                            'Доступен',
                            '/img/noavatar.png'
                        ));
                        deferred.resolve(request.info);
                    } else if (request.status.substr(0,1) === "4")
                        deferred.reject(request.info)
                    else
                        this.error(request.info);
                },
                // Ответ не получен
                error: function(code) {
                    if (typeof code == "undefined") code = 'Время вышло, сервер не ответил.';
                    show_notice('error', code, 30);
                    deferred.reject();
                },
            }).chain_uuid
        };
        Skar.socket.send(CBOR.encode(data));

        return out;
    },
    // событие изменения данных одного из отслеживаемых пользователей
    changedUserData: function(data) {
        if (typeof data["login"] !== "undefined") {
            Skar.getUserData(data["login"], false, false).done(function(user) {
                for (var key in data["update"]) {
                    var new_value = data["update"][key]

                    for(var i in Skar.contacts)
                        if (Skar.contacts[i].login === user.login)
                            Skar.contacts[i][key] = new_value;

                    if (key == "status") {

                        // Ищем страницу с диалогом
                        var pageid = 'dialog'+user.uid;
                        if (new_value == "online") {
                            if (typeof Skar.pages[pageid] !== "undefined") {
                                Skar.pages[pageid].dom.children("header").addClass("online")
                            }
                            DOM.contacts.children('#'+user.uid).addClass("online");
                        } else {
                            if (typeof Skar.pages[pageid] !== "undefined") {
                                Skar.pages[pageid].dom.children("header").removeClass("online")
                            }
                            DOM.contacts.children('#'+user.uid).removeClass("online");
                        }
                    } else {
                        // Другие данные. Имя, current_info, аватарка.
                    }

                }
            }).fail(function(code){
                show_notice(
                    'error',
                    "При обработке уведомления о изменении данных одного из ваших контактов возника ошибка: "+code,
                     30
                );
            })
        } else {
            Skar.user = $.extend(Skar.user, data["update"]);
            // Обновляем мини-профиль
            DOM.minprofile.html(Template.minprofile(
                typeof Skar.user.name !== "undefined" && Skar.user.name ? Skar.user.name : Skar.user.login,
                'Доступен',
                '/img/noavatar.png'
            ));
        }
    },
    // Открывает профиль пользователя(используя данные из Skar.user)
    openMyProfile: function() {
        if (typeof Skar.pages.myprofile === "undefined") {
            // Добавляем страницу
            Skar.pages.myprofile = {
                dom: $(Template.section("myprofile")).insertAfter($('section').last())
            }
        }
        Skar.pages.myprofile.dom.html(Template.myprofile(Skar.user));
        // Создаем обработчик скролла
        var outputscroll = Skar.pages.myprofile.dom.find('output').jScrollPane({
            stickToBottom: true,
            contentWidth:100
        });
        Skar.pages.myprofile.scroll = outputscroll.data('jsp');

        // Показываем страницу настройки
        $('section').hide();
        Skar.pages.myprofile.dom.show();
        Skar.page = "myprofile";
        DOM.contacts.children("div").removeClass("current");

        document.title = "Мой профиль - Skar.IM"
        addUrl('myprofile', document.title);

        DOM.reinitialise();
    },
    openDialog: function(login, use_history) {
        if (typeof use_history === "undefined") use_history = false;

        // Пропускать, если диалог уже открыт
        if (DOM.contacts.children('.current').attr("data-login") == login)
            return;

        Skar.getUserData(login).done(function(user){
            // Ищем страницу с диалогом
            var pageid = 'dialog'+user.uid;
            // Если не нашли, то создаем её.
            if (typeof Skar.pages[pageid] === "undefined") {
                Skar.createDialog(user,pageid);
            }
            var contact = DOM.contacts.children('#'+user.uid);
            DOM.contacts.children("div").removeClass("current");
            if (typeof contact.attr("data-newmsg") !== "undefined")
                Skar.unread--;
            DrawFavicon(Skar.unread);
            contact.removeAttr("data-newmsg").addClass("current");

            // Показываем диалог
            $('section').hide();
            Skar.pages[pageid].dom.show();
            Skar.page = pageid;

            if (typeof user.name !== "undefined")
                document.title = user.name+' — Skar.IM'
            else
                document.title = user.login+' — Skar.IM'

            if (!use_history) {
                shlog = login.split("/");
                if (shlog[0]=="skaro::"+host)
                    shlog = shlog[1];
                else
                    shlog = shlog.join('/');

                addUrl(shlog, document.title, login);
            }

            DOM.reinitialise();
        });
    },
    createDialog: function(user,pageid) {

        // Создаем страницу диалога(пока без истории)
        Skar.pages[pageid] = {
            dom: $(Template.dialog(user.login,user.uid,user.name,user.status)).insertAfter($('section').last())
        }
        // Устанавливаем обработчик события для отправки по Enter и переносу строки по Shift+Enter (инициирует событие onsubmit для отправки)
        Skar.pages[pageid].dom.find("footer form textarea").unbind("keydown").bind("keydown",ShiftEnterLine);

        // Dropdowns
        Skar.pages[pageid].dom.find(".dropdown-button").bind("click", function() {
            var panel = $('#'+$(this).data("activates"));
            console.log(panel);
            if (panel.css('display') == 'none') {
                panel.css('display', 'block');
                panel.css('opacity', 0);
                panel.animate({opacity: 1, bottom: '+=10px'}, 100);
            } else {
                panel.animate({opacity: 0, bottom: '-=10px'}, 50, function(){panel.css('display', 'none');});
            }
        });


        // Если это новый контакт(нет в контактах), то просто выводим пустой диалог. Т.к. совместной истории еще нет
        Skar.updateHistory(user.login,pageid);
    },
    // Добавление пользователя в список контактов
    addContact: function(login) {
        // Создаем Deferred обьект, для того, чтобы выводить результат функции в callback
        var deferred = $.Deferred(),
            out = deferred.promise()

        var data = {
            type: "add_contact",
            login: login,
            chain_uuid: new Request({
                // Сколько ждать (сек)
                timeout: 10,
                // Пришел ответ сервера
                success: function(request) {
                    if (request.status.substr(0,3) === "200") {
                        Skar.initUserData(request.user);
                        deferred.resolve(request.user);
                    } else {
                        this.error(request.info);
                    }
                },
                // Ответ не получен
                error: function(code) {
                    if (typeof code == "undefined") code = 'Время вышло, сервер не ответил.';
                    show_notice('error', code, 30);
                    deferred.reject(code);
                },
            }).chain_uuid
        };
        Skar.socket.send(CBOR.encode(data));

        return out;
    },
    delContact: function(login) {
        // Создаем Deferred обьект, для того, чтобы выводить результат функции в callback
        var deferred = $.Deferred(),
            out = deferred.promise()

        var data = {
            type: "del_contact",
            login: login,
            chain_uuid: new Request({
                // Сколько ждать (сек)
                timeout: 10,
                // Пришел ответ сервера
                data: {
                    login: login
                },
                success: function(request) {
                    if (request.status.substr(0,3) === "200") {
                        for(var i in Skar.contacts) {
                            if (this.data.login == Skar.contacts[i].login) {
                                // Ищем страницу с диалогом и удаляем её
                                var pageid = 'dialog'+Skar.contacts[i].uid;
                                if (typeof Skar.pages[pageid] !== "undefined") {
                                    Skar.pages[pageid].dom.remove();
                                    delete Skar.pages[pageid];
                                }
                                // Потом удаляем из контактов
                                DOM.contacts.children('#'+Skar.contacts[i].uid).remove();

                                delete Skar.contacts[i];
                                delete Skar.user.contacts[this.data.login];

                                deferred.resolve(request);
                            }
                        }
                    } else {
                        this.error(request.info);
                    }
                },
                // Ответ не получен
                error: function(code) {
                    if (typeof code == "undefined") code = 'Время вышло, сервер не ответил.';
                    show_notice('error', code, 30);
                    deferred.reject(code);
                },
            }).chain_uuid
        };

        Skar.socket.send(CBOR.encode(data));
        return out;
    },
    checkInContacts: function(login) {

        for (var i in Skar.contacts) {
            if (login == Skar.contacts[i].login) {
                return Skar.contacts[i];
            }
        }
        return false
    },
    openSearchContact: function(part_of_login) {
        // Создаем страницу, если еще не создана
        if (typeof Skar.pages.search == "undefined") {
            Skar.pages.search = {
                dom: $(Template.searchPage()).insertAfter($('section').last())
            }
            var outputscroll = Skar.pages.search.dom.find('output').jScrollPane({
                contentWidth: 100
            });
            Skar.pages.search.scroll = outputscroll.data('jsp');
        }

        DOM.contacts.children("div").removeClass("current");
        $('section').hide();
        Skar.pages.search.dom.show();
        Skar.page = 'search';

        document.title = 'Поиск — Skar.IM'
        addUrl('search', document.title, part_of_login);


        $("#search_field").off("keyup").on("keyup", Skar.updateSearchContacts);

        DOM.reinitialise();
    },
    updateSearchContacts: function() {
        var deferred = $.Deferred(),
            out = deferred.promise(),
            field = $(this),
            part_of_login = this.value;

        if (part_of_login) {
            var data = {
                type: "search_contacts",
                query: part_of_login,
                chain_uuid: new Request({
                    // Сколько ждать (сек)
                    timeout: 10,
                    // Пришел ответ сервера
                    success: function(request) {
                        if (request.status.substr(0,3) === "200") {

                            Skar.pages.search.dom.find("output").html(Template.search_results(request.result));
                            DOM.reinitialise();

                            deferred.resolve(request.result);
                        } else {
                            this.error(request.info);
                        }
                    },
                    // Ответ не получен
                    error: function(code) {
                        if (typeof code == "undefined") code = 'Время вышло, сервер не ответил.';
                        show_notice('error', code, 30);
                        deferred.reject(code);
                    }
                }).chain_uuid
            }
            Skar.socket.send(CBOR.encode(data));
        } else {
            deferred.reject();
        }

        return out;
    },
    getUserData: function(login, add_contact, load_if_empty) {
        if (typeof add_contact === "undefined") add_contact = true;
        if (typeof load_if_empty === "undefined") load_if_empty = true;

        // Создаем Deferred обьект, для того, чтобы выводить результат функции в callback
        var deferred = $.Deferred(),
            out = deferred.promise();

        // Если мы ищем самого себя
        if (login === Skar.user.login) {
            deferred.resolve(Skar.user);
            return out;
        }

        // Если одного из контактов
        var user = false;
        // Ищем пользователя в списке контактов
        for (var i in Skar.contacts) {
            if (login === Skar.contacts[i].login) {
                deferred.resolve(Skar.contacts[i])
                return out;
                break;
            }
        }
        // Кого-то еще.
        if (user === false) {
            if (!load_if_empty) {
                deferred.reject("Пользователь не является вашим контактом");
                return out;
            }

            var data = {
                type: "get_user_data",
                login: login,
                chain_uuid: new Request({
                    // Сколько ждать (сек)
                    timeout: 10,
                    // Пришел ответ сервера
                    success: function(request) {
                        if (request.status.substr(0,3) === "200") {
                            // Добавляем в список контактов
                            if (add_contact) {
                                request.data = Skar.initUserData(request.data);
                            }

                            deferred.resolve(request.data);
                        } else {
                            this.error(request.info);
                        }
                    },
                    // Ответ не получен
                    error: function(code) {
                        if (typeof code == "undefined") code = 'Время вышло, сервер не ответил.';
                        show_notice('error', code, 30);
                        deferred.reject(code);
                    }
                }).chain_uuid
            }
            Skar.socket.send(CBOR.encode(data));
        }

        return out;
    },
    // Добавление пользователя в список контактов
    getContacts: function() {
        // Создаем Deferred обьект, для того, чтобы выводить результат функции в callback
        var deferred = $.Deferred(),
            out = deferred.promise()

        var data = {
            type: "get_contacts",
            chain_uuid: new Request({
                // Сколько ждать (сек)
                timeout: 10,
                // Пришел ответ сервера
                success: function(request) {
                    if (request.status.substr(0,3) === "200") {
                        for (var contact in request.data)
                            Skar.initUserData(request.data[contact]);
                        deferred.resolve(request.data);
                    } else {
                        this.error(request.info);
                    }
                },
                // Ответ не получен
                error: function(code) {
                    if (typeof code == "undefined") code = 'Время вышло, сервер не ответил.';
                    show_notice('error', code, 30);
                    deferred.reject(code);
                },
            }).chain_uuid
        };
        Skar.socket.send(CBOR.encode(data));

        return out;
    },
    // инициализирует пользователя или конференцию как контакт, если нужно.
    initUserData: function (data, settings) {
        if (typeof settings == "undefined") settings = {}

        skar_contact = false;
        for(var i in Skar.contacts) {
            if (Skar.contacts[i].login == data.login) {
                skar_contact = true;
            }
        }
        if (!("uid" in data))
            data.uid = Uniq.getID();

        if (!skar_contact) {
            if (typeof Skar.user.contacts[data.login] == "undefined" || settings.length)
                Skar.user.contacts[data.login] = settings

            Skar.contacts.push(data);

            DOM.contacts.prepend($(Template.contact(data, Skar.user.contacts[data.login])));
        }

        return data;
    },
    openUserProfile: function(login) {
        // TODO: показываем лоадер
        Skar.getUserData(login,false).done(function(user) {
            DOM.chatInfo.html(Template.getprofile(user));
            DOM.rswipeOpen();
        }).always(function() {
            // TODO: закрываем лоадер
        });
    },
    openDialogInfo: function(login) {
        if (DOM.chatInfo.find(' > div').hasClass('chatinfo')) {
            DOM.rswipeTrigger();
        } else {
            /* TODO: Нужно получать текущие настройки диалога и передавать их в шаблон на вывод
            Кроме того, нужно повесить обработчики на события onchange для полей формы */
            DOM.chatInfo.html(Template.chatinfo(login));
            DOM.rswipeTrigger();
        }
    },
};
