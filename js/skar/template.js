/**
Templates
*/

Template = {
    section: function(id,data) {
        if (typeof data == "undefined") data = '';
        return  '<section id='+id+'>'+data+'</section>';
    },
    headerNonbg: function(title) {
        return  '<header class="nonbg">'+
                    '<div class="left lswipe waves-effect"><div class="infoicon closeicon"></div></div>'+
                    ((typeof title !== "undefined") ? '<b>'+title+'</b>' : '')+
                '</header>';
    },
    loading: function() {
        return  '<div class="loading"><div>'+
                    '<div class="icon-spinner8 rotating"></div>'+
                '</div></div>';
    },
    loginform: function(register_type) {
        html =  '<form class="center relative h100p" action="" method="post" id="loginform">'+
                    '<div class="window">'+
                        '<div class="block">'+
                            '<h4 class="light">Авторизация</h4>'+
                            '<div class="input-field">'+
                                '<input class="validate" type="text" name="login" placeholder="Логин или адрес почты" autocapitalize="off" autocorrect="off" required/>'+
                                '<input class="validate" type="password" name="password" placeholder="Пароль" required/>'+
                            '</div>'+
                            '<input class="btn-large" type="submit" value="Войти"/>'+
                        '</div>'+
                        '<div class="links">';

        if (register_type == 'open' || register_type == 'invite' && getUrlParams().invite) {
            html +=         '<a id="reset_pass" class="left" href="#">Забыли пароль?</a>'+
                            '<a id="register" class="right" href="#">Создать аккаунт</a>';
        } else {
            html +=         '<a id="reset_pass" href="#">Забыли пароль?</a>';
        }

        html +=         '</div>'+
                    '</div>'+
                '</form>';
        return html;
    },
    registerform: function() {
        return  '<form class="center relative h100p" action="" method="post" id="registerform">'+
                    '<div class="window">'+
                        '<div class="block">'+
                            '<h4 class="light">Создать аккаунт</h4>'+
                            '<div class="input-field">'+
                            '<input class="validate" type="email" name="email" placeholder="Электронная почта" required/>'+
                            '<input class="validate" type="text" name="login" placeholder="Логин"/>'+
                            '<input class="validate" type="password" name="password" placeholder="Пароль" required/>'+
                            '</div>'+
                            '<input class="btn-large" type="submit" value="Регистрация"/>'+
                        '</div>'+
                        '<div class="links">'+
                            '<a id="signin" href="#">Уже есть аккаунт? Войти.</a>'+
                        '</div>'+
                    '</div>'+
                '</form>';
    },
    message: function(msg,waiting,continued) {
        if (typeof waiting === "undefined") waiting = false;
        if (waiting)
            waiting = ' waiting" id="w';
        else
            waiting = '" id="m';

        if (typeof continued === "undefined") continued = false;
        if (continued)
            continued = ' continued';
        else
            continued = '';

        if (msg.from.login == Skar.user.login)
            to = ' out animated bounceInRight';
        else
            to = ' in animated bounceInLeft';

        return  '<div class="message'+continued+to+waiting+msg.mid+'" data-login="'+msg.from.login+'">'+
                    (continued ? '' : '<img src="/img/noavatar.png"'+(msg.from.login !== Skar.user.login ? ' onclick="Skar.openUserProfile(\''+msg.from.login+'\')" class="pointer"' : '')+'/>')+
                    '<div class="headmsg"><!--<h3 class="left">'+msg.from.name+'</h3>--></div>'+
                    '<div class="bodymsg">'+
                        '<div class="text fz1 ib">'+
                            msg.body+
                        '</div>'+
                        '<div class="footer"><time>'+msg.date+'</time></div>'+
                    '</div>'+
                '</div>';
    },
    dialog: function(login,uid,name,online) {
        if (typeof online === "undefined") online = 'offline';
        if (typeof name === "undefined" || !name) name = login;

        return  '<section id="dialog'+uid+'" style="display:none">'+
                    '<header'+(online !== 'offline' ? ' class="online"' : '')+'>'+
                        '<nav>'+
                            '<div class="nav-wrapper">'+
                                '<div class="left lswipe waves-effect"><div class="infoicon closeicon"></div></div>'+
                                '<span>'+name+'</span>'+
                                '<ul class="right hide-on-med-and-down">'+
                                    '<li class="pointer waves-effect" onclick="Skar.openUserProfile(\''+login+'\')" ><i class="material-icons left right">account_circle</i></li>'+
                                    '<li class="pointer waves-effect" onclick="Skar.openDialogInfo(\''+login+'\')"><i class="material-icons left right">more_vert</i></li>'+
                                '</ul>'+
                            '</div>'+
                        '</nav>'+
                    '</header>'+
                    '<output>'+
                    '</output>'+
                    '<footer class="z-depth-1">'+
                        '<form action="" method="post" class="m0 relative" onsubmit="Skar.sendMessage($(this),\''+login+'\');return false;">'+
                            '<div class="textarea">'+
                                '<textarea placeholder="напишите сообщение..." name="body" class="materialize-textarea" style="height: 25px;" required></textarea>'+
                            '</div>'+

                            '<div id="panel-apps'+uid+'" class="panel panel-apps">'+
                                '<div>'+
                                    '<div class="waves-effect imgbox left icon-mic w50p pointer"></div>'+
                                    '<div class="waves-effect imgbox left icon-video-camera w50p pointer"></div>'+
                                    '<div class="waves-effect imgbox left icon-upload2 w25p pointer"></div>'+
                                    '<div class="waves-effect imgbox left icon-cancel-circle w25p pointer"></div>'+ /* Яндекс.Диск */
                                    '<div class="waves-effect imgbox left icon-cancel-circle w25p pointer"></div>'+ /* Google Drive */
                                    '<div class="waves-effect imgbox left icon-cancel-circle w25p pointer"></div>'+ /* Dropbox */
                                    '<div class="waves-effect imgbox left icon-cancel-circle w50p pointer"></div>'+ /* Markdown */
                                    '<div class="waves-effect imgbox left icon-cancel-circle w50p pointer"></div>'+ /* Code */
                                    '<div class="waves-effect textbox left w100p pointer">Добавить участников</div>'+
                                '</div>'+
                            '</div>'+
                            '<div id="panel-smile'+uid+'" class="panel panel-smile">'+
                                '<div>'+
                                    'смайлы'+
                                '</div>'+
                            '</div>'+
                            '<div class="pointer ib up dropdown-button waves-effect" data-activates="panel-apps'+uid+'" style="left:6px;"><i class="material-icons">apps</i></div>'+
                            '<div class="pointer ib up dropdown-button waves-effect" data-activates="panel-smile'+uid+'" style="left:40px;"><i class="material-icons">tag_faces</i></div>'+

                            '<label class="pointer right up waves-effect" style="right: 7px;">'+
                                '<input type="submit" style="display:none"/>'+
                                '<i class="material-icons">send</i>'+
                            '</label>'+
                        '</form>'+
                    '</footer>'+
                '</section>';
    },
    chatinfo: function(login,current) {
        return  '<div class="chatinfo center p1 h100p valign-wrapper white-text teal">\
                    <div class="valign row">\
                        <div class="row">\
                            <div class="switch row">\
                                <label class="white-text">'+
                                    'Уведомления'+
                                    '<input disabled type="checkbox">'+
                                    '<span class="lever"></span>'+
                                '</label>'+
                            '</div>\
                            <div class="switch row">\
                                <label class="white-text">'+
                                    'Архивация'+
                                    '<input disabled type="checkbox">'+
                                    '<span class="lever"></span>'+
                                '</label>'+
                            '</div>\
                        </div>\
                        <div class="divider row"></div>\
                        <div>\
                            <span class="btn-floating red waves-effect waves-light tooltipped-chatinfo" data-tooltip="Удалить чат" onclick="Skar.delContact(\''+login+'\')"><i class="small material-icons">delete</i></span>\
                        </div>\
                    </div>\
                </div>\
                <script>\
                    $(document).ready(function(){\
                        $(\'.tooltipped-chatinfo\').tooltip({position:"bottom",delay: 50});\
                      });\
                </script>';
    },
    myprofile: function(user) {
        return  '<header class="nonbg">'+
                    '<nav>'+
                        '<div class="nav-wrapper">'+
                            '<div class="left lswipe waves-effect"><div class="infoicon closeicon"></div></div>'+
                            '<span>Мой профиль</span>'+
                            '<ul class="right">'+
                                '<li class="pointer waves-effect hide" onclick="deactiveCE(this, $(\'#myprofile .editable-container\'))"><i class="material-icons left right">check</i></li>'+
                                '<li class="pointer waves-effect" onclick="activeCE(this, $(\'#myprofile .editable-container\'), Skar.setMyProfileData)"><i class="material-icons left right">edit</i></li>'+
                            '</ul>'+
                        '</div>'+
                    '</nav>'+
                '</header>'+
                '<output class="row">'+
                    '<div>'+
                        '<div class="card z-depth-0 col s12">\
                            <div class="card-image myprofile" style="height:200px">\
                                <img class="abs_center" style="width: auto;max-width: 100%;" src="https://pbs.twimg.com/profile_banners/1712670223/1401698057/1500x500">\
                                <i class="material-icons right black-text small circle white relative">edit</i>\
                            </div>\
                            <div class="card-content">\
                                <div class="imgbox myprofile__avatar-float">\
                                    <img class="circle" src="/img/noavatar.png"/>\
                                </div>\
                                <div class="row">\
                                    <div data-name="name" class="editable-container disabled">\
                                        <span ContentEditable="false" class="editable-value card-title grey-text text-darken-4">'+user.name+'</span>\
                                        <ul class="editable-buttons">\
                                            <li class="editable-buttons__back-button hide"><i class="material-icons waves-effect small">close</i></li>\
                                        </ul>\
                                    </div>\
                                    <div data-name="login" class="editable-container disabled">\
                                        <span ContentEditable="false" class="editable-value fz2">'+ user.login +'</span>\
                                        <ul class="editable-buttons">\
                                            <li class="editable-buttons__back-button hide"><i class="material-icons waves-effect small">close</i></li>\
                                        </ul>\
                                    </div>\
                                    <div data-name="current_info" class="editable-container disabled">\
                                        <span ContentEditable="false" class="editable-value fz1">'+user.current_info+'</span>\
                                        <ul class="editable-buttons">\
                                            <li class="editable-buttons__back-button hide"><i class="material-icons waves-effect small">close</i></li>\
                                        </ul>\
                                    </div>\
                                </div>\
                                <div class="row">'+JSON.stringify(user.accounts)+'</div>\
                            </div>\
                        </div>'+
                    '</div>'+
                '</output>';
    },
    getprofile: function(user) {
        return  '<div class="center p1 blue white-text h100p valign-wrapper">'+
                    '<div class="valign w100p">'+
                        '<h5 class="light">'+user.name+'</h5>'+
                        '<div class="row"><img style="width: 100px;height: 100px;" class="circle" src="/img/noavatar.png"/></div>'+
                        '<p class="thin fz1">'+user.login+'<br/>'+user.current_info+'</p>'+
                        '<div class="row">'+
                            (
                            (Skar.checkInContacts(user.login) === false) ?
                            '<span data-tooltip="Добавить в контакты" class="btn-floating green waves-effect waves-light tooltipped-userinfo" onclick="Skar.addContact(\''+user.login+'\').done(function(request) {Skar.openMyProfile();})"><i class="small material-icons">person_add</i></span> '
                            :
                            '<span data-tooltip="Удалить из контактов" class="btn-floating red waves-effect waves-light tooltipped-userinfo" onclick="Skar.delContact(\''+user.login+'\').done(function(request) {Skar.openMyProfile();})"><i class="small material-icons">delete</i></span> \
                            <span data-tooltip="Отключить уведомления" class="disabled btn-floating grey waves-effect waves-light tooltipped-userinfo" onclick=""><i class="small material-icons">notifications_off</i></span>'
                            )+
                        '</div>'+
                        //'<div class="row">'+JSON.stringify(user.accounts)+'</div>'+
                    '</div>'+
                '</div>\
                <script>\
                    $(document).ready(function(){\
                        $(\'.tooltipped-userinfo\').tooltip({position:"bottom",delay: 50});\
                      });\
                </script>';
    },
    minprofile: function(name,status,avatar) {
        return  '<div onclick="Skar.openMyProfile();">'+
                    '<div class="imgbox left circleblock">'+
                        '<img src="'+avatar+'"/>'+
                    '</div>'+
                    '<span>'+name+'</span><br/>'+
                    '<small>'+status+'</small>'+
                '</div>';
    },
    contact: function(data, settings) {
        if (typeof data.status == "undefined") data.status = "offline";
        if (typeof settings.type == "undefined") settings.type = "contact";
        // TODO: нужно еще маленький восклицательный знак выводить. Мол ошибка при получении информации о пользователе
        if (typeof data.name === "undefined" || !data.name) data.name = data.login;


        var classes = [];
        if (data.status !== "offline") classes.push("online");
        if (settings.type == "conference") classes.push("showOptions");


        return  '<div id="'+data.uid+'" data-login="'+data.login+'" class="collection-item avatar waves-effect'+(classes.length > 0 ? ' '+classes.join(' ') : '')+'">'+
                    '<img src="/img/noavatar.png" alt="" class="circle">'+
                    '<span class="title truncate">'+data.name+'</span>'+
                    ((settings.type == "conference") ? '<sup><i class="material-icons tiny blue-text">group</i></sup>' : '')+
                '</div>';
    },
    searchPage: function() {

        return  '<section id="search" style="display:none">'+
                    '<header>'+
                        '<nav>'+
                            '<div class="nav-wrapper">\
                                <div class="input-field search-nav-block">\
                                    <input id="search_field" type="search" required>\
                                    <div class="left lswipe waves-effect"><div class="infoicon closeicon"></div></div>\
                                    <i class="material-icons right left">search</i>\
                                </div>\
                            </div>'+
                        '</nav>'+
                    '</header>'+
                    '<output class="row">'+

                    '</output>'+
                '</section>';
    },
    search_results: function(results) {
        var results_html = [];
        for( var i in results ) {
            var res = results[i];
            results_html.push(
                '<li class="collection-item avatar">\
                  <img src="/img/noavatar.png" alt="" class="circle">\
                  <span class="title">'+res.name+' (Похож на '+Math.round((res.match_val||0.001)*10000)/100+'%)</span>\
                  <p>'+res.login+' <br>\
                     '+res.current_info+'\
                  </p>'+
                  ((Skar.checkInContacts(res.login) === false) ?
                  '<a href="#!" class="secondary-content" onclick="Skar.addContact(\''+res.login+'\')"><i class="material-icons">person_add</i></a>' : '') +
                '</li>'
            );
        }
        if (!results_html.length) {
            results_html.push(
                '<li class="collection-item">\
                  <span class="title fz1">Поиск ничего не нашел <i class="material-icons left">mood_bad</i></span>\
                 </li>'
            );
        }
        return '<div class="m1"><ul class="collection">'+results_html.join('')+'</ul></div>';
    },
    errorForOutput: function(info,does) {
        return  '<div class="error">'+
                    '<div>'+info+'</div>'+
                    '<button onclick="'+does+'">Попробовать снова</button>'+
                '</div>';
    },
    notyWarning: function(text,icon_class) {
        if (typeof icon_class == "undefined") icon_class = 'error_outline';
        return '<i class="material-icons left small">'+icon_class+'</i><span class="fz2 tiny">'+text+'</span>';
    }
};
// На лучшие времена...
Template.notyError = Template.notyWarning;
Template.notyInformation = Template.notyWarning;
Template.notySuccess = Template.notyWarning;
