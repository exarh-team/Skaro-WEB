map $http_upgrade $connection_upgrade {
    default upgrade;
    '' close;
}

# Balancing Tornado servers for WebSocket
upstream websocket {
    server 127.0.0.1:8000 max_fails=1 fail_timeout=5s;
}

server {
    listen       80;
    server_name  localhost;
    
    charset utf-8;

    set $path_to_static /opt/skaro-server/static;
    
    access_log  /var/log/nginx/skaro-server-access.log;
    error_log   /var/log/nginx/skaro-server-error.log;
    
    # For static files output
    location / {
        root   $path_to_static;
        index  index.html index.htm;
    }
    
    #error_page  404              /404.html;

    # redirect server error pages to the static page /50x.html
    
    error_page   500 502 503 504  /50x.html;
    location = /50x.html {
        root   $path_to_static;
    }

    location ~ ^/(((@|~)([\w\d_\-]+))|(([\w]+)::(.+))|myprofile|search)$ {
        rewrite .* /index.html?$args;
    }
    
    # Proxy Tornado WebSocket on ws://localhost/skaro_client/ or /skaro_server/
    location ~ ^/skaro_(client|server)/$ {
        proxy_pass http://websocket;
        
        #When many WS servers
        #proxy_next_upstream     error timeout invalid_header http_500;
        
        proxy_connect_timeout   2;
        # Max connection time 24 hours
        proxy_read_timeout 86400s;
        
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection $connection_upgrade;
    }
}
